package com.inteligr8.junit4;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Assert;

public class AssertRegex {
	
	public static void assertMatches(String expectedRegex, String actual) {
		assertMatches(null, expectedRegex, actual);
	}
	
	public static void assertMatches(String message, String expectedRegex, String actual) {
		Pattern pattern = Pattern.compile(expectedRegex);
		assertMatches(message, pattern, actual);
	}
	
	public static void assertMatches(Pattern expected, String actual) {
		assertMatches(null, expected, actual);
	}
	
	public static void assertMatches(String message, Pattern expected, String actual) {
		assertMatches(message, false, expected, actual);
	}
	
	public static void assertNotMatches(String expectedRegex, String actual) {
		assertNotMatches(null, expectedRegex, actual);
	}
	
	public static void assertNotMatches(String message, String expectedRegex, String actual) {
		Pattern pattern = Pattern.compile(expectedRegex);
		assertNotMatches(message, pattern, actual);
	}
	
	public static void assertNotMatches(Pattern expected, String actual) {
		assertNotMatches(null, expected, actual);
	}
	
	public static void assertNotMatches(String message, Pattern expected, String actual) {
		assertMatches(message, true, expected, actual);
	}
	
	private static void assertMatches(String message, boolean negate, Pattern expected, String actual) {
		Matcher matcher = expected.matcher(actual);
		if (matcher.matches() == negate) {
			message = message == null ? "" : (message + "; ");
			message += "expression of expected string: <" + expected.toString() + "> but actual string: <" + actual + ">";
			Assert.fail(message);
		}
	}
	
	
	
	public static void assertFind(String expectedRegex, int expectedCount, String actual) {
		assertFind(null, expectedRegex, expectedCount, actual);
	}
	
	public static void assertFind(String message, String expectedRegex, int expectedCount, String actual) {
		Pattern pattern = Pattern.compile(expectedRegex);
		assertFind(message, pattern, expectedCount, actual);
	}
	
	public static void assertFind(Pattern expected, int expectedCount, String actual) {
		assertFind(null, expected, expectedCount, actual);
	}
	
	public static void assertFind(String message, Pattern expected, int expectedCount, String actual) {
		Matcher matcher = expected.matcher(actual);
		int count = 0;
		for (count = 0; matcher.find(); count++)
			;
		if (expectedCount < 0) Assert.assertTrue(message, -expectedCount <= count);
		else Assert.assertEquals(message, expectedCount, count);
	}

}
