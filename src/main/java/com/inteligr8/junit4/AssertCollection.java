package com.inteligr8.junit4;

import java.util.Collection;

import org.junit.Assert;

public class AssertCollection {
	
	public static void assertSize(int expectedCount, Collection<?> actualCollection) {
		assertSize(null, expectedCount, actualCollection);
	}
	
	public static void assertSize(String message, int expectedCount, Collection<?> actualCollection) {
		if (expectedCount == 0) Assert.assertTrue(message, actualCollection == null || actualCollection.isEmpty());
		else Assert.assertTrue(message, actualCollection != null && actualCollection.size() == expectedCount);
	}
	
	public static <T> void assertContains(T expectedElement, Collection<T> actualCollection) {
		assertContains(null, expectedElement, actualCollection);
	}
	
	public static <T> void assertContains(Collection<T> expectedElements, Collection<T> actualCollection) {
		assertContains(null, expectedElements, actualCollection);
	}
	
	public static <T> void assertContains(String message, T expectedElement, Collection<T> actualCollection) {
		Assert.assertTrue(message, actualCollection != null && actualCollection.contains(expectedElement));
	}
	
	public static <T> void assertContains(String message, Collection<T> expectedElements, Collection<T> actualCollection) {
		Assert.assertTrue(message, actualCollection != null && actualCollection.containsAll(expectedElements));
	}

}
